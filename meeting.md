---
sidebar: false
---

# Meeting Notes

Recent on the top.
Please use [URL shortener](https://gitlab.com/kawalcovid19/website/kcov.id) to make it more readable

* [Rapat 13 Maret](http://kcov.id/brief0313)
* [Rapat 12 Maret](http://kcov.id/brief0312)
* [Rapat 11 Maret](http://kcov.id/brief0311)
* [Rapat 10 Maret](http://kcov.id/brief0310)
* [2020-03-06 Basic AI First Call](https://docs.google.com/document/d/1zM2XnCrub33g-4v4ct2_1UwRCvnSDvaSVoAe0yd9Wyc/edit)

## Meeting Plan

| Date | Topic|Link to Join|Host|
| ----------- | ----------- |----------- |--|
| 27 March | Lorem Ipsum |Lorem Ipsum|tim pm|
| Lorem Ipsum |Lorem Ipsum |Lorem Ipsum|lorem|
