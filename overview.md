# kawalcovid19 website

## Latar Belakang

Banyaknya simpang siur informasi baik yang disengaja maupun tidak disengaja di dunia maya Indonesia terkait topik covid-19 menimbulkan kecemasan apa yang harus dilakukan masyarakat Indonesia dalam menghadapi situasi. Di sisi lain, kanal informasi dari pemerintah dirasakan kurang mampu mengatasi kecemasan ini [*citation needed*]. Oleh karena itu, dibutuhkan sebuah kanal informasi yang mampu menjawab pertanyaan masyarakat terkait covid-19 serta mampu menjadi pelurus misinformasi/disinformasi di masyarakat terkait covid-19.

## Tujuan

Sumber informasi terpercaya dan tepat guna terkait 5w 1h covid-19 di Indonesia.

Mengutip [pernyataan](https://t.me/c/1412792802/128) dari Mas @ainunnajib:
> fundamental difference antara KawalCOVID19 dan MAFINDO/TurnBackHoax: \
> objektif KawalCOVID19 menyediakan informasi paling terpercaya (termasuk hoax busting kalau perlu) \
> objektif MAFINDO menyisir semua informasi yang beredar yang ditengarai hoax \
> kalau MAFINDO itu BPPOM, KawalCOVID19 itu restoran

## Keywords

Informasi, verifikasi, edukasi, misinformasi, hoax, tim verifikator, draft konten

## Strategi

Untuk mencapai tujuan tersebut, maka website harus memiliki 3 komponen:

    - Informasi
        Berisi fakta terkait covid-19 didukung dengan referensi yang kuat
    - Verifikasi
        Berisi klarifikasi atau sanggahan terhadap hoax/isu seputar covid-19. Klarifikasi juga harus didukung dengan referensi yang kuat
    - Edukasi
        Berisi informasi yang mampu (harus/boleh/tidak boleh) dilakukan oleh masyarakat untuk dapat menghadapi covid-19 dengan baik. Bisa berupa tips, petunjuk, atau tata cara pelaksanaan suatu metode tertentu. Harus didukung dengan referensi yang kuat

Untuk memastikan informasi-informasi yang dinaikkan ke website jelas pengelompokkannya serta memiliki dukungan referensi yang mumpuni, dibutuhkan alur verifikasi serta tim verifikator yang akan menyaring, memilah, dan membubuhkan referensi untuk setiap draft konten yang masuk. Draft konten bisa bersumber dari:

    1. lembaga yang berwenang terkait topik covid-19.
        Bisa Kementerian Kesehatan RI, WHO, atau lembaga lain yang memiliki kewenangan atau kemampuan setara
    2. Isu atau hoax seputar covid-19 di masyarakat.
    3. etc

Tim verifikator akan mengolah draft konten tersebut dan akan memutuskan apakah draft konten akan dianggap sebagai:

    1. Fakta. Analisis, referensi dan penelusuran fakta dari tim verifikator menyimpulkan bahwa draft adalah benar adanya.
    2. Hoax/misinformasi. Analisis, referensi dan penelusuran fakta dari tim verifikator menyimpulkan bahwa draft adalah tidak benar. Draft konten yang dianggap hoax harus memiliki sanggahan / klarifikasi.

## Kebutuhan fitur

Untuk mendukung ketersampaian strategi kawalcovid19, maka dibutuhkan fungsi-fungsi:

    1. Website utama. Berisi outlet dari proses verifikasi baik itu Fakta atau Hoax yang dikelompokkan ke komponen Informasi, Verifikasi, atau Edukasi. Akses bersifat publik.
    2. CMS workflow management. Berisi CMS yang akan diakses oleh tim verifikator atau tim internal lain yang berkepentingan. Akses bersifat terbatas.

Selain fungsional utama tersebut, website kawalcovid19 diharapkan memiliki:

    1. SEO yang bagus sehingga memudahkan warganet Indonesia dalam menemukan situs ini di mesin pencari.
    2. Keandalan yang tinggi. Website utama memiliki calon audien seluruh warganet Indonesia sehingga website harus siap menerima traffic dalam jumlah besar dan spike tiba-tiba ketika muncul isu baru seputar covid-19.
    3. Keamanan yang kuat. Akan selalu ada aktor-aktor nakal yang berniat merusak sistem kawalcovid19. Rusaknya integritas informasi karena serangan dari luar akan menghilangkan kepercayaan masyarakat terhadap website maupun tim kawalcovid19

## Perkiraan Timeline Pemenuhan Fitur*

Menimbang cukup kompleksnya fitur yang harus dipenuhi oleh website kawalcovid19 dan latar belakang dari tim tech yang swadaya, maka timeline pemenuhan fitur website kawalcovid19 akan sebagai berikut (*TBD*)

    1. Website Utama saja
        Di posisi ini website hanya bersifat sebagai outlet dari tim verifikator. Tim verifikator memroses konten dan melakukan verifikasi dengan alur manual
    2. CMS workflow management
        Di posisi ini sudah muncul CMS yang bisa digunakan oleh tim verifikator. CMS dan website utama belum terhubung.
    3. Integrasi Website Utama dan CMS.
        Konten Website Utama akan diupdate otomatis oleh CMS.

## Organization Chart

Daftar ini dibuat dengan bantuan [vuejs-tree](https://github.com/scalia/vuejs-tree)

<!-- markdownlint-disable no-inline-html -->
<organization-chart />
<!-- markdownlint-enable no-inline-html -->
