# Integrasi GitLab dan Slack

*Workspace* Slack kita sudah terintegrasi dengan GitLab sebagai *Version Control System* dan CI/CD yang kita gunakan untuk pengembangan perangkat lunak dalam setiap timnya. Terdapat 2 metode/arah integrasi yang dimungkinkan yaitu:

- Melakukan berbagai perintah GitLab (membuat *issue*, melihat *issue*, menutup *issue*, *deployment*, dll.) yang biasanya dilakukan melalui situs GitLab terhadap suatu *project*/repositori tertentu. Bagian ini selanjutnya akan disebut sebagai `GitLab Slack application`. Contohnya adalah seperti gambar di bawah ini.
![Contoh GitLab Slack application](./images/gitlab-slack-application.png)

- Mendapat notifikasi di *channel* Slack tertentu terkait hal-hal yang terjadi (adanya *issue* baru, *Merge Request* yang di-*merged*, ada *issue* yang ditutup, tes/*build*/*deployment* yang gagal, dll.) pada suatu *project*/repositori GitLab tertentu. Bagian ini selanjutnya akan disebut sebagai `GitLab Slack notification`. Contohnya adalah seperti gambar di bawah ini.
![Contoh GitLab Slack notification](./images/gitlab-slack-notification.png)

## GitLab Slack application

### Pemakaian GitLab Slack application

`GitLab Slack application` dapat digunakan dengan menggunakan `slash commands` berawalan `/gitlab` dengan bentuk lengkap `/gitlab <project-name> <command> <body>` dan diketik di *channel* **manapun** seperti kamu berbincang di *channel* Slack dengan syarat repositori `<project-name>` di GitLab sudah mengaktifkan `Slack application` di bagian `Integration`.

- Gunakan `/gitlab help` untuk melihat semua `slash commands` yang memungkinkan untuk dijalankan di dalam Slack.
- Daftar lengkap dari semua `slash commands` yang dimungkinkan dapat dilihat pada halaman [ini](https://gitlab.com/help/integration/slash_commands.md).
- Beberapa contohnya adalah `/gitlab kawal-diri issue new build frontend`untuk membuat *issue* baru di repositori/*project* `kawal-diri` dengan judul *issue* `build frontend` atau `/gitlab self-diagnose issue show 3` untuk menampilkan *issue* nomor 3 dari repository/*project* `self-diagnose`.

### *Setup* GitLab Slack application

Terdapat beberapa langkah yang butuh untuk dijalankan sebelum sebuah repositori/*project* dapat diubah *state*-nya melalui *slash commands* di Slack atau yang kita sebut dengan `GitLab Slack application` yaitu:

- Buka halaman *Integration* pada *project*/repositori yang ingin diintegrasikan ke dalam Slack yakni dengan **Project > Settings > Integrations*.
- Pergi ke bagian *Slack application* dan tekan tombol *Add to Slack* untuk mengintegrasikan repositori/*project* tersebut ke Slack dengan `GitLab Slack application`.
- Setelah memastikan bahwa *workspace* Slack yang akan diintegrasikan adalah *workspace* KawalCOVID19, tekan tombol *allow* untuk memperbolehkan.
- Kamu dapat mengubah nama *project* atau yang disebut juga dengan *project alias* yang akan diacu sebagai `project-name` pada `slash commands` yang nantinya akan diketik di Slack *channel*. Rekomendasinya adalah gunakan cukup nama *project*/repositorinya saja. Contohnya ubah dari `kawalcovid19/ppe-marketplace/ppe-marketplace-backend` menjadi `ppe-marketplace-backend` dengan menekan tombol *edit*.
- Jika kamu melihat gambar seperti di bawah maka kamu telah berhasil melakukan *setup* untuk `GitLab Slack application` untuk *project*/repositori tersebut.

![GitLab Slack application succeed](./images/gitlab-slack-application-succeed.png)

## GitLab Slack notification

### Pemakaian GitLab Slack notification

Kamu akan mendapatkan notifikasi di *channel* yang telah didefinisikan sesuai dengan kejadian atau perubahan status yang diatur pada bagian *setup* dari integrasi *project*/repositori terhadap *workspace* Slack.

### *Setup* GitLab Slack notification

Terdapat beberapa langkah yang butuh untuk dijalankan sebelum sebuah *channel* Slack bisa mendapatkan notifikasi ketika terdapat perubahan *state* atau aksi yang terjadi dari sebuah repositori/*project* GitLab atau yang kita sebut dengan `GitLab Slack notification` yaitu:

- Dari *workspace* Slack, ke bagian kiri atas di mana terdapat tulisan KawalCOVID19 dan *username* Slackmu, klik, **Administration > Manage apps > Custom Integrations > Incoming WebHooks > Add to Slack**.
- Pilih *channel* yang akan menjadi tujuan notifikasi, sesuaikan repositori/*project* dengan *channel* dari tim *project* tersebut. Misalnya notifikasi repositori `ppe-marketplace/ppe-marketplace-backend` dikirimkan ke channel `#team-ppe-marketplace`. Klik tombol `Add Incoming WebHooks integration` untuk mendapatkan WebHook URL.
- Salin dan simpan `WebHook URL` dalam bentuk `https://hooks.slack.com/services/string1/string2`. Di halaman ini kamu juga dapat mengkustomisasi beberapa opsi pada bagian `Integration settings` di bagian bawah seperti label deskriptif, mengubah nama, mengubah mengubah ikon, dan lainnya.
- Buka halaman *Integration* pada *project*/repositori GitLab yang ingin diintegrasikan ke dalam Slack yakni dengan **Project > Settings > Integrations > Slack notifications*.
- *Paste* `WebHook URL` yang tadi telan disalin ke bagian form `Webhook` di bagian bawah.
- Di sini kamu juga dapat mengkustomisasi beberapa opsi sesuai kebutuhan tim dan *project* atau agar tidak terlalu banyak pesan yang menumpuk pada *channel* tim. Misalnya tim kamu hanya ingin mendapatkan *update* perihal *issue* dan bukan *push* maka *checkbox* *push* dapat dihilangkan centangnya. Atau juga *channel* dapat di-*override* dari pilihan yang sudah ditentukan ketika melakukan *setup* di Slack di tahap sebelumnya. Kustomisasi yang biasanya dilakukan yaitu:

  - *Trigger* *push* diisi dengan *channel* `gl-push`.
  - *Trigger* *note* dan *confidential note* diisi dengan *channel* `gl-comments`.
  - *Trigger* *pipeline* diisi dengan *channel* `gl-pipeline`.
  - *Username* diisi dengan nama `kawalcovid19-<nama-tim/*project*>`.
  - *Branches to be notified* diisi dengan *All branches*.

- Jika sudah selesai melakukan kustomisasi, tekan tombol `Test settings and save changes` untuk memfinalisasi integrasi.
- Jika kamu melihat gambar seperti di bawah maka kamu telah berhasil melakukan *setup* untuk `GitLab Slack notification` untuk *project*/repositori tersebut.

![GitLab Slack notification succeed](./images/gitlab-slack-notification-succeed.png)
