# Architecture

## Diagram

Diagram ini dibuat dengan menggunakan syntax [mermaid](https://docs.gitlab.com/ee/user/markdown.html#mermaid)

```mermaid
graph TD
	subgraph "Public Users"
		WU([Web User])
		MU([WIP: Mobile User])
	end

	subgraph "Internal Users"
		V([Volunteer])
	end

	subgraph "Netlify"
		WU -->|Access| N(Static Site)
	end

	subgraph "Azure API Management"
		MU -->|Call| CaseAPI(Case Summary API)
		MU -->|Call| ContentAPI(Content API)
		N -->|Call| CaseAPI(Case Summary API)
	end

	subgraph "Now"
		MU -->|Access| API(Static API)
	end

	subgraph "Heroku"
		N -->|Fetch Periodically| WP[App Server]
		API -->|Fetch Periodically| WP[App Server]
	end

	subgraph "Google"
		V -->|Discuss Content| GD(Google Docs)
		V -->|Publish Content| GD(Google Docs)
		V -->|Update Stats| GS(Google Sheets)
	end

	subgraph "API Backend Server - Azure App"
		CaseAPI -->|Call| WS(Web Server)
		ContentAPI -->|Call| WS(Web Server)
		GS -->|Fetch every 10 minutes| SJ(Scheduled Job)
	end

	subgraph "Azure"
		WP -->|Connect| DB[(Database)]
		WP -->|Connect| ST[(Storage)]
		SJ -->|Writes| DB
		DB -->|Reads| WS
	end

	subgraph "Tech Team"
		GD -->|Content Source| A([WordPress Admin])
		A -->|Publish Content| WP
	end
```
