# Posisi Yang Tersedia

| Posisi | Status|Kontak PIC|TIM|Short Job Description|
| ----------- | ----------- |----------- |-----------|-----------|
| Product Manager (2 orang) | Available |Kartika Dwi Baswara| #team-marketplace-ppe|handle 2 squad: squad marketplace (terdiri dari 2 platform: vendor platform dan marketplace platform utk dipake sama RS), dan squad ecommerce curation (untuk nge-daily curate di tiap-tiap ecommerce platform, toko mana yang harganya reasonable)|
| UI Designer| Available |Alfi Noor| #survey-subteam|bikin self-diagnose untuk mobile view dan desktop view|
|UX researchers|Available|Cornelius Vito|UXResearch needed for Kawal Diri|Track users 24/7|
|UX Writer|Available|Rizqi Nino Firmansyah|#expert-design|need UX Writer for CTA and Thankyou Static Page, also who can illustration for any page at Kawal Diri apps|
| UX | Open Position | Rendy B. junior |  #team-fatality-news | We would like to present fataility news to our users along with its context. Fatality news is the most accurate signal / noise-free in under testing situations and when the government covers details of death cases. It also spreads fast because bad news tends to travel faster than good news. |
| Designer | Available | Ibad Saladdin | #team-kawal-diri | develop squad baru, yaitu sharing and virality. Scopenya mainly on customising sharing informasi di app kawal diri dan refer new connection.|
| Tim Ops | Available | Muhammad Rizqi | #team-needs-matchmaking | notify ke orang yg mau kasih bantuan aja lewat whatsapp |
| Deploy | Available | Muhammad Yusuf atau Alip Sidik P |  #team-needs-matchmaking | React sama Go, buat databasenya (rencananya) kita pake Cosmos DB |
| Front End | Open Position | Ichwan | #team-ppe-marketplace | implementasi di bagian frontend (tech stack - react Typescript) |
| Tim infografis | Open Position | Mimi | | Membuat Infografis |
| Koordinator data entry | 1 Orang | Grace, Ronald Bessie |  | Memastikan semua relawan memasukkan data hari itu ke dalam tabel, mengecheck keakuratan data. Berkoordinasi dengan Koordinator Data (Ronald Bessie) |
| Data Entry | 22 Orang | Grace, Ronald Bessie | | Memasukkan data di setiap provinsi yang sudah dipilih setiap hari secara teratur dan cermat. |
